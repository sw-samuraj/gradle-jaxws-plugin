# Gradle JAX-WS Plugin Changelog #

## 0.6.1 ##

Bugfix version.

### Bugfixes ###

*  Wrong `wsdlLocation` for JAR archive.

### New Features ###

* Two JAR archive examples - one for convention-compliant project, one for custom configured.

## 0.6.0 ##

Feature version.

### New Features ###

* Configuration option for `wsdlLocation` has been added.

## 0.5.0 ##

Feature version.

### New Features ###

* Configuration option for package name has been added.
* Configuration option for extension has been added.

## 0.4.1 ##

Bugfix version.

### Bugfixes ###

* Other than default WSDL directory cause ignoring of `wsImport` task.

## 0.4.0 ##

Feature version.

### New Features ###

* `wsClean` task for deleting of generated artifacts.
* Different *default* WSDL location for `war` and `java` projects.
* Suppressed *Ant* logging from `wsImport` task.

## 0.3.0 ##

Feature version.

### New Features ###

* New `xjc` task for generating Java artifacts from XSD.
* New example for `xjc` task.

## 0.2.0 ##

Feature version.

### New Features ###

* JAXB binding is optional.
* Adds `LICENSE.txt` file to the `META-INF` directory of the builded JAR file.
* Adds `CHANGELOG.md` file for tracking of changes.

## 0.1.1 ##

Bugfix version.

### Bugfixes ###

* Java classes don't compile when generatedSources is configured in the task.

### New Features ###

* Adds *extension* object `JaxwsPluginExtension` for configuring of the plugin.

## 0.1.0 ##

Initial version.

### New Features ###

* Generating Java artifacts from WSDL.
* Usage of JAXB binding for generating of *Bean Validation* annotations.
