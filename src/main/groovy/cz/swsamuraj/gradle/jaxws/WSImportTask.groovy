/*
 * Copyright (c) 2015, Vít Kotačka
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package cz.swsamuraj.gradle.jaxws

import org.gradle.api.DefaultTask
import org.gradle.api.file.FileTree
import org.gradle.api.plugins.WarPlugin
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class WSImportTask extends DefaultTask {

    String group = 'Web Services'

    String description = 'Generates Java classes from WSDL.'

    @InputDirectory
    File wsdlDir

    @OutputDirectory
    File generatedSources

    @TaskAction
    def wsImport() {
        FileTree wsdls = project.fileTree(getWsdlDir()).include('*.wsdl')

        getGeneratedSources().mkdirs()

        wsdls.visit { wsdl ->
            def wsName = wsdl.name - 'Service.wsdl'

            ant.taskdef(name: 'wsimport',
                        classname: 'com.sun.tools.ws.ant.WsImport',
                        classpath: project.configurations.jaxws.asPath)

            def params = [keep: true,
                          sourcedestdir: generatedSources,
                          wsdl: "${wsdlDir}/${wsdl.name}",
                          xnocompile: true]

            if (!project.jaxws.wsdlLocationDefault) {
                if (project.plugins.hasPlugin(WarPlugin)) {
                    params.put('wsdllocation', "WEB-INF/wsdl/${wsdl.name}")
                } else {
                    params.put('wsdllocation', "META-INF/wsdl/${wsdl.name}")
                }
            }

            def bindingFile = "${wsdlDir}/binding/${wsName}Service.xjb"

            if (project.file(bindingFile).exists()) {
                params.put('binding', bindingFile)
            }

            if (project.jaxws.packageName) {
                params.put('package', project.jaxws.packageName)
            }

            if (project.jaxws.extension) {
                params.put('extension', project.jaxws.extension)
            }

            ant.wsimport(params) {
                xjcarg(value: '-Xannotate')
            }
        }
    }

}
