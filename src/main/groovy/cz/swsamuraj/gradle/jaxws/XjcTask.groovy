/*
 * Copyright (c) 2015, Vít Kotačka
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package cz.swsamuraj.gradle.jaxws

import org.gradle.api.DefaultTask
import org.gradle.api.file.FileTree
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class XjcTask extends DefaultTask {

    String group = 'Web Services'

    String description = 'Generates Java classes from XSD.'

    @InputDirectory
    File xsdDir

    @OutputDirectory
    File generatedSources

    @TaskAction
    def xjc() {
        FileTree xsds = project.fileTree(getXsdDir()).include('*.xsd')

        getGeneratedSources().mkdirs()

        xsds.visit { xsd ->
            def xsdName = xsd.name - '.xsd'

            ant.taskdef(name: 'xjc',
                        classname: 'org.jvnet.jaxb2_commons.xjc.XJC2Task',
                        classpath: project.configurations.jaxws.asPath)

            def params = [schema: "${xsdDir}/${xsd.name}",
                          destdir: generatedSources]

            def bindingFile = "${xsdDir}/binding/${xsdName}.xjb"

            if (project.file(bindingFile).exists()) {
                params.put('extension', true)
                params.put('binding', bindingFile)
            }

            ant.xjc(params) {
                arg(value: '-Xannotate')
            }
        }
    }

}
