# Gradle JAX-WS Plugin #

Contract-first approach for SOAP based Web Services by generating Java artifacts from WSDL with optional usage of JAXB binding.

## Applying the Plugin ##

### Gradle 2.1+ ###

```
#!groovy
plugins {
  id "cz.swsamuraj.jaxws" version "0.6.1"
}
```

### All Gradle Versions ###

```
#!groovy
buildscript {
  repositories {
    maven {
      url "https://plugins.gradle.org/m2/"
    }
  }
  dependencies {
    classpath "gradle.plugin.cz.swsamuraj:gradle-jaxws-plugin:0.6.1"
  }
}

apply plugin: "cz.swsamuraj.jaxws"
```

## Using the Plugin ##

### Tasks ###

**wsImport**

`wsImport` task takes all the WSDL files from given directory (either the default, or the configured one) and process them by `wsimport` utility. The task generates just Java source code with no compilation. Generated source code is then compiled by standard `compileJava` task.

**xjc**

`xjc` task takes all the XSD files from given directory (either the default, or the configured one) and process them by `xjc` utility. The task generates just Java source code with no compilation.

This task is not hooked to the `compileJava` task. It has to be either explicitly called, or when is't hooked to the build lifecycle, then the `wsImport` task has to be disabled to avoid a collision between the tasks. See *Config options* below, or the *Examples*.

**wsClean**

`wsClean` task deletes a directory defined in `generatedSources` configuration option.

### Default Project Layout ###

**wsImport**

* a directory where the `wsImport` task searches for WSDL file(s):
    * `src/main/webapp/WEB-INF/wsdl` for project with applied `war` plugin, 
    * `src/main/resources/META-INF/wsdl` for other kind of projects.
* `src/main/webapp/WEB-INF/wsdl/binding` (or `src/main/resources/META-INF/wsdl/binding` respectively) a directory where the `wsImport` task searches for JAXB binding file(s).
* `src/generated/jaxws` a directory where the plugin stores generated JAX-WS artifacts.

**xjc**

* `src/main/resources/META-INF/xsd` a directory where the `xjc` task searches for XSD file(s).
* `src/main/resources/META-INF/xsd/binding` a directory where the `xjc` task searches for JAXB binding file(s).
* `src/generated/xjc` a directory where the task stores generated JAXB artifacts.

### Conventions ###

The plugin expects following conventions:

**wsImport**

* Given WSDL file has suffix `Service.wsdl`, e.g. `InvoiceService.wsdl`.
* Given JAXB binding file has suffix `Service.xjb`, e.g. `InvoiceService.xjb`.
* The convention which is just optional is for *XML Schema* files when they are externalised from WSDL - they should be in the `wsdl/xsd` directory with suffix `Service.xsd`, e.g. `InvoiceService.xsd`.

An example with the convention and default layout:

```
#!shell
guido@sandman:$ tree src
src
└── main
    └── webapp
        └── WEB-INF
            └── wsdl
                ├── InvoiceService.wsdl
                ├── binding
                │   └── InvoiceService.xjb
                └── xsd
                    └── InvoiceService.xsd

6 directories, 3 files
```

**xjc**

* Given XSD file can have any name with the `xsd` file extension, e.g. `Order.xsd`.
* Given JAXB binding file has to have the same name as the XSD file and the file extension `xjb`, e.g. `Order.xjb`.

An example with the convention and default layout:

```
#!shell
src
└── main
    └── resources
        └── META-INF
            └── xsd
                ├── binding
                │   └── Order.xjb
                └── Order.xsd
```

### Config options ###

**wsImport**

* `wsdlDir` a directory where the plugin searches for WSDL file(s).
* `wsdlLocationDefault` a boolean switch, if enabled, the default `wsdlLocation` value will be used (usually a full path to WSDL file).
* `generatedSources` a directory where the plugin stores generated JAX-WS artifacts.
* `packageName` a package of the generated sources (instead of `targetNamespace`).
* `extension` a boolean value (default `false`), enables vendor extensions (functionality not specified by the specification).

An example of the configuration:

```
#!groovy
jaxws {
    wsdlDir = 'src/main/resources/META-INF/wsdl'
    wsdlLocationDefault = true
    generatedSources = 'src/gen/ws'
    packageName = 'my.special.pckg'
    extension = true
}
```

**xjc**

* `xsdDir` a directory where the plugin searches for XSD file(s).
* `generatedSources` a directory where the plugin stores generated JAXB artifacts.

An example of the configuration:

```
#!groovy
xjc {
    xsdDir = 'src/xsd'
    generatedSources = 'src/gen/jaxb'
}
```

If the `xjc` task need to be hooked to the build lifecycle, it can be done by following configuration:

```
#!groovy
compileJava.dependsOn tasks.xjc
wsImport.enabled = false
```

### Build Lifecycle ###

The plugin sets a dependency of standard Java task `compileJava` for `wsImport` task. That means that the WSDL/XSD artifacts are generated before compilation of Java sources. This hook up is not set for the `xjc` task, so it has to be called either explicitly, or when is't hooked to the build lifecycle, then the `wsImport` task has to be disabled to avoid a collision between the tasks. See *Config options* below, or the *Examples*.

The plugin implements an *incremental build* - the `wsImport` and `xjc` tasks proceeds only if WSDL/XSD file(s) has/have changed, or if the directory with generated sources has been deleted, otherwise the tasks are marked as *UP-TO-DATE*.

An example of the task lifecycle output:

```
#!shell
guido@sandman:$ gradle war
:wsImport
:compileJava
:processResources UP-TO-DATE
:classes
:war

BUILD SUCCESSFUL

Total time: 3.028 secs
```

## Multi-project Build ##

The plugin can be used with multi-project builds.

Based on specific configuration, there could be a constraint: The default WSDL directory differs whether the `war` plugin is applied or not. If `allprojects` or `subprojects` script block is used, then the `wsdlDir` default value will be set like for *non-war* project, even if the `war` plugin is applied to the project later. In such case, `wsImport` task is marked as `SKIPPED` and Java artifacts are not generated.

**Possible solutions:**

Define project with `war` plugin before `allprojects` or `subprojects` definitions, e.g.:

```
#!groovy
project(':web') {
    apply plugin: 'war'
}

subprojects {
    apply plugin: 'cz.swsamuraj.jaxws'

    // rest of the definitions
}
```

Apply plugin selectively, e.g.:

```
#!groovy
TBD
```

## Examples ##

Usage of the plugin and example project can be found in the `examples` directory:

* `wsimport-war-example` for usage of the `wsimport` task on web project.
* `wsimport-jar-example` for usage of the `wsimport` task on Java (jar) project.
* `wsimport-jar-custom-example` for usage of the `wsimport` task on Java (jar) projectj with some custom configuration.
* `xjc-example` for usage of the `xjc` task.

## TODO ##

* See [issues](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/issues?status=new&status=open) for upcoming enhancements.

## Contribution guidelines ##

I'll be happy with any participation or feedback. Please, feel free to:

* [fork](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/fork) the project
* send me a [pull request](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/pull-requests/new), but please, follow *hg flow* guide below
* [track](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/issues?status=new&status=open) a bug, or an enhancement
* or anything else what can improve this plugin.

## Pull Requests with HG Flow ##

This repository is using [HG Flow](https://bitbucket.org/yujiewu/hgflow/overview) for branch and release management. I would appreciate if you will use following guideline for providing of [pull requests](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/pull-requests/):

1. Create a new [issue](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/issues?status=new&status=open).
1. Clone the repository.
1. Create a new feature branch with the name of the issue ID: e.g. `feature/42`. You can create such branch with the command: `hg flow feature start <issue ID>`.
1. Do your changes.
1. Create a new [pull request](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/pull-requests/) from your feature branch to my default (development) branch.

If this is too complicated for you, or you are not experienced with *HG Flow*, you can follow another approach:

1. Create a new [issue](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/issues?status=new&status=open) and let me know in the comment to create a new feature branch for you.
1. Clone the repository and do your changes. I will create the feature branch in the meantime.
1. Create a new [pull request](https://bitbucket.org/sw-samuraj/gradle-jaxws-plugin/pull-requests/) from your repository to my feature branch.

## License ##

The **gradle-jaxws-plugin** is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.