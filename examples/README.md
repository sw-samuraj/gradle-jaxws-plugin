# JAX-WS Plugin Examples #

## How to Build Examples ##

All example projects are configured through the only one `build.gradle` file, stored in the examples root directory.

The examples can be built by command:

```
gradle clean wsClean build
```

Generated source codes are stored in the `src/generated/jaxws`, resp. `src/generated/xjc` directory. Compiled Java classes are stored in the `build/classes/main` directory.

## Examples Content ##

### wsimport-war-example ###

A web project example with generating of Java sources from WSDL file with usage of JAXB binding. Bean Validation annotations are applied through the binding too.

### wsimport-jar-example ###

A Java project example with generating of Java sources from WSDL file with usage of JAXB binding. Bean Validation annotations are applied through the binding too.

### wsimport-jar-custom-example ###

A Java project example with generating of Java sources from WSDL file with usage of JAXB binding. Bean Validation annotations are applied through the binding too.

The project is configured to use different than the default WSDL location and some other custom changes deviating from the used convention.

### xjc-example ###

A Java project example with generating of Java sources from XSD file with usage of JAXB binding. Bean Validation annotations are applied through the binding too.

The project is configured so that `xjc` tasks is hooked to the `compileJava` task and the `wsImport` task is disabled to avoid collision between the tasks.

## TODO ##

* `wsimport-war-example` should be runnable through the *Jetty*.
* Web Service in the `wsimport-war-example` should be callable through the prepared *SoapUI* project.
